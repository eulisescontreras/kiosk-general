import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LobbyHomePage } from './pages/lobby-home/lobby-home.page';
import { WhereToEatPage } from './pages/where-to-eat/where-to-eat.page';
import { DrinksPage } from './pages/drinks/drinks.page';
import { CreateOrderPage } from './pages/create-order/create-order.page';
import { OrderReviewPage } from './pages/order-review/order-review.page';
import { PaymentTypePage } from './pages/payment-type/payment-type.page';
import { OrderCompletedPage } from './pages/order-completed/order-completed.page';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  { 
    path: '',
    component: LobbyHomePage
  },
  { 
    path: 'where-to-eat',
    component: WhereToEatPage
  },
  { 
    path: 'drinks',
    component: DrinksPage
  },
  { 
    path: 'create-order',
    component: CreateOrderPage
  },
  { 
    path: 'order-review',
    component: OrderReviewPage
  },
  { 
    path: 'payment-type',
    component: PaymentTypePage
  },
  { 
    path: 'order-completed',
    component: OrderCompletedPage
  },
  /*{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },*/
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
