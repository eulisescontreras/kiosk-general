import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { NgDragDropModule } from 'ng-drag-drop';

import { LobbyHomePage } from './pages/lobby-home/lobby-home.page';
import { WhereToEatPage } from './pages/where-to-eat/where-to-eat.page';
import { DrinksPage } from './pages/drinks/drinks.page';
import { CreateOrderPage } from './pages/create-order/create-order.page';
import { OrderReviewPage } from './pages/order-review/order-review.page';
import { PaymentTypePage } from './pages/payment-type/payment-type.page';
import { OrderCompletedPage } from './pages/order-completed/order-completed.page';
import { BackButtonComponent } from './components/back-button/back-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LobbyHomePage,
    WhereToEatPage,
    DrinksPage,
    CreateOrderPage,
    OrderReviewPage,
    PaymentTypePage,
    OrderCompletedPage,
    BackButtonComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    NgDragDropModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
