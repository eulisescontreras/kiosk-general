import { AnimationController } from '@ionic/angular';
import { AfterViewInit, Component, ElementRef, NgZone, OnInit, QueryList, ViewChild, ViewChildren, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.page.html',
  styleUrls: ['./create-order.page.css']
})
export class CreateOrderPage implements OnInit, AfterViewInit  {

  @ViewChildren('items') items: QueryList<any>;
  @ViewChild('glass', { static: true }) glass: ElementRef;

  foods = [];
  foodTypes = [
    {
      name: 'Pizzas',
      image: 'assets/images/lobby/pizza.png'
    },
    {
      name: 'Ensaladas',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Pastas',
      image: 'assets/images/lobby/pastas.png'
    },
    {
      name: 'Aperitivos',
      image: 'assets/images/lobby/appetizers.png'
    },
    {
      name: 'Bebidas',
      image: 'assets/images/lobby/drinks.png'
    }
  ];
  toppings = [
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    },
    {
      name: 'Salads',
      image: 'assets/images/lobby/salads.png'
    }
  ];
  sizes = [
    '10\'\'', '12\'\'', '14\'\'', '16\'\''
  ];
  typeCuts = [
    {
      id: 1,
      name: '8 Slides',
      icon: 'assets/images/8slides.png'
    },
    {
      id: 2,
      name: '10 Slides',
      icon: 'assets/images/10slides.png'
    },
    {
      id: 3,
      name: '16 Slides',
      icon: 'assets/images/16slides.png'
    },
    {
      id: 4,
      name: 'Ajedrez Slides',
      icon: 'assets/images/ajedrezslides.png'
    }
  ];

  typeCut = null;
  size = null;

  innerWidth: number

  constructor(
    private animationCtrl: AnimationController,
    private router: Router,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    this.ngZone.run(() => {
      setTimeout(() => {
        if (this.glass) {
          this.animationCtrl.create()
            .addElement(this.glass.nativeElement)
            .duration(400)
            .fromTo('opacity', '1', '0')
            .play().then(() => {
              this.glass.nativeElement.style.display = 'none';
            });
        }
      }, 2000);
    });
    
    for (let index = 0; index < 8; index++) {
      const food: any = {
        name: 'Hawaiian',
        price: '$10.99',
        isOpen: false,
        toppings: []
      };
      this.foods.push(food);
    }
  }

  ngAfterViewInit() {
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = event.target.innerWidth;
  }

  setTypeCut(cut) {
    this.typeCut = cut;
  }

  setSize(size) { 
    this.size = size;
  }

  onDragEnter(event: any, item: any, side: string) {
    event.path[7].classList.add(side);
  }

  onDragLeave(event: any, item:any, side: string) {
    const topping = item.toppings.find(t => t.side === side);
    if (!topping) {
      event.path[7].classList.remove(side);
    }
  }

  onDrop(event: any, item: any, side: string) {
    item.toppings.push({
      ...event.dragData,
      side
    });
  }

  open(item: any, index: number) { 
    item.isOpen = !item.isOpen;
    if (item.isOpen) {
      this.animationCtrl.create()
        .addElement(this.items.toArray()[index].nativeElement.children[2])
        .duration(200)
        .fromTo('height', '0px', '192px')
        .play()
    } else {
      this.animationCtrl.create()
        .addElement(this.items.toArray()[index].nativeElement.children[2])
        .duration(200)
        .fromTo('height', '192px', '0px')
        .play()
    }
  }

  cancelOrder() {
    this.router.navigate(['/where-to-eat']);
  }

  confirmOrder() {
    this.router.navigate(['/drinks']);
  }

}
