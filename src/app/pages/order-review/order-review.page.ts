import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-review',
  templateUrl: './order-review.page.html',
  styleUrls: ['./order-review.page.css']
})
export class OrderReviewPage implements OnInit {

  order: any = {
    id: '0000',
    foods: [
      {
        name: 'Hawaiian',
        size: 'S',
        quantity: 1,
        price: '$19.99',
        toppings: [
          {
            name: 'BBQ',
            part: 'FIRST_HALF'
          },
          {
            name: 'Hot sauce',
            part: 'SECOND_HALF'
          },
          {
            name: 'Pepperoni',
            part: 'ALL'
          },
          {
            name: 'Cheddar',
            part: 'ALL'
          },
          {
            name: 'Pepinillo',
            part: 'ALL'
          }
        ]
      }
    ],
    taxes: '$1.56',
    discount: '$10.99',
    total: '$25.96'
  };

  constructor(private router: Router) { }

  ngOnInit() {
  }

  getPart(part: string) {
    switch (part) {
      case 'FIRST_HALF':
        return 'assets/images/lobby/first-half.png';
        break;
      case 'SECOND_HALF':
        return 'assets/images/lobby/second-half.png';
        break;
      case 'ALL':
        return 'assets/images/lobby/all.png';
        break;
      default:
        break;
    }
  }

  cancel() {
    this.router.navigate(['/drinks']);
  }

  confirm() {
    this.router.navigate(['/payment-type']);
  }

}
