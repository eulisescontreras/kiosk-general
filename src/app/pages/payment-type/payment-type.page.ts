import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-type',
  templateUrl: './payment-type.page.html',
  styleUrls: ['./payment-type.page.css']
})
export class PaymentTypePage implements OnInit {

  paymentType: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/order-review']);
  }

  setPaymentType(paymentType: string) {
    this.paymentType = paymentType;
    this.router.navigate(['/order-completed']);
  }

}
