// import { animate, query, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  /*animations: [
    trigger('fadeAnimation', [
      transition('* => *', [
        query(':enter',
          [
            style({ opacity: 0 })
          ],
          { optional: true }
        ),
        query(':leave',
          [
            style({ opacity: 1 }),
            animate('0.3s', style({ opacity: 0 }))
          ],
          { optional: true }
        ),
        query(':enter',
          [
            style({ opacity: 0 }),
            animate('0.3s', style({ opacity: 1 }))
          ],
          { optional: true }
        )
      ])
    ])
  ]*/
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  /*getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }*/

}
