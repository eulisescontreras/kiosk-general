import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-where-to-eat',
  templateUrl: './where-to-eat.page.html',
  styleUrls: ['./where-to-eat.page.css']
})
export class WhereToEatPage implements OnInit {

  where = [
    {
      id: 'EAT_IN',
      name: 'Eat in',
      icon: 'assets/images/lobby/eat-in.svg'
    },
    {
      id: 'TAKE_AWAY',
      name: 'Take away',
      icon: 'assets/images/lobby/take-away.svg'
    },
    {
      id: 'DELIVERY',
      name: 'Delivery',
      icon: 'assets/images/lobby/delivery.svg'
    }
  ];

  languages = [
    {
      locale: 'en',
      name: 'English'
    },
    {
      locale: 'es',
      name: 'Español'
    }
  ];

  whereToEat: string;
  language: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  setWhereToEat(where: string) {
    this.whereToEat = where;
    this.router.navigate(['/create-order']);
  }

  setLanguage(lang: string) {
    this.language = lang;
  }

}
