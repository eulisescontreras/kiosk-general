import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.page.html',
  styleUrls: ['./drinks.page.css']
})
export class DrinksPage implements OnInit {

  drinks = [0, 1, 2, 3, 4, 5, 6, 7];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/create-order']);
  }

  confirm() {
    this.router.navigate(['/order-review']);
  }

}
